# -*- coding: utf-8 -*-


import json

from django.shortcuts import render,redirect,HttpResponse

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from app_auth.views import Perms_required
from app_log import models
from app_auth.models import User_log




@login_required
@Perms_required
def audit(request):
    title = "操作审计"
    audit_obj = models.Audit.objects.all().order_by("-start_time")
    audit_list = []
    for i in audit_obj:
        audit_list.append({'host_ip':i.host_ip,'hostname':i.hostname,'user_name':i.user_name,'start_time':i.start_time,'audit_id':i.id})
    return render(request,"log_audit.html", locals())


@csrf_exempt
def audit_check(request):
    if request.method == "POST":
        audit_id = request.POST.get('audit_id')
        audit_obj = models.Audit.objects.get(id=audit_id)
        audit_log = audit_obj.audit_log
        return HttpResponse(audit_log)


@login_required
@Perms_required
def user_log(request):
    title = "用户日志"
    log_obj = User_log.objects.all().order_by("-add_time")
    userlog_list = []
    for i in log_obj:
        userlog_list.append({'username':i.username,'url_title':i.url_title,'active':i.active,'status':i.status,'add_time':i.add_time})
    return render(request,"log_user.html", locals())