# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-07-10 09:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_log', '0002_auto_20180710_1032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='audit',
            name='user_phone',
        ),
    ]
