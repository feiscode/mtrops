# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class User_log(models.Model):
    username = models.CharField(max_length=32)
    url_title = models.CharField(max_length=64)
    active = models.CharField(max_length=32)
    status = models.CharField(max_length=32)
    add_time = models.DateTimeField(auto_now_add=True)
    def __unicode__(self):
        return self.username

