
from django.conf.urls import url,include
from app_tool import views


urlpatterns = [
    url('^elk/$', views.ELK),
    url('^nessus/$', views.Nessus),
    url('^zabbix/$', views.ZABBIX),
    url('^upfile/$', views.Upfile),
    url('^downfile/$', views.Downfile),
    url('^webssh/$', views.Webssh),
    url('^phpmyadmin/$', views.phpmyadmin),
]

