#### mtrops ####



#### 平台架构 ####





#### 环境依赖（python 2.7） ####

1. django==1.11.9
2. ELK                  #日志管理         
3. webssh               #web终端
4. openpyxl             #excel文件处理
5. saltstack            #自动化实现
6. Mysql-python
7. pymysql
8. nessus               #漏洞扫描
9. phpmyadmin           #mysql web客户端
10. django-cerely
11. redis



#### 项目部署 ####

https://gitee.com/12x/mtrops/wikis/%E9%A1%B9%E7%9B%AE%E9%83%A8%E7%BD%B2


#### 使用说明 ####

1. 默认登录用户密码：admin admin




#### 项目介绍 ####
 **登录界面** 
![登录界面](https://gitee.com/uploads/images/2018/0620/150427_c7b3a516_578265.png "屏幕截图.png")
 **首页** 
![首页](https://images.gitee.com/uploads/images/2018/0814/115532_2bed255c_578265.png "屏幕截图.png")
 **资产清单** 
![资产清单](https://images.gitee.com/uploads/images/2018/0814/115612_594d8e69_578265.png "屏幕截图.png")
 **主机详细信息** 
![资产详细信息](https://images.gitee.com/uploads/images/2018/0814/115643_25f15afc_578265.png "屏幕截图.png")
 **代码发布** 
![代码发布](https://images.gitee.com/uploads/images/2018/0814/115731_06dbe6f8_578265.png "屏幕截图.png")
 **代码回滚** 
![单位回滚](https://images.gitee.com/uploads/images/2018/0828/154331_9eb97739_578265.png "屏幕截图.png")
 **文件管理** 
![文件管理](https://images.gitee.com/uploads/images/2018/0821/114201_3d13a218_578265.png "屏幕截图.png")
 **批量管理** 
![批量管理](https://images.gitee.com/uploads/images/2018/0905/152444_5a1a4937_578265.png "屏幕截图.png")
 **计划任务** 
![计划任务](https://images.gitee.com/uploads/images/2018/0918/104039_55c30a81_578265.png "屏幕截图.png")
 **权限管理** 
![权限管理](https://gitee.com/uploads/images/2018/0620/150728_4abde699_578265.png "屏幕截图.png")
 **webshell** 
![webshell](https://gitee.com/uploads/images/2018/0620/150808_f676accc_578265.png "屏幕截图.png")
 **堡垒机审计** 
![堡垒机审计](https://images.gitee.com/uploads/images/2018/0712/114946_ffcd1751_578265.png "屏幕截图.png")
 **审计日志记录** 
![审计日志](https://images.gitee.com/uploads/images/2018/0712/115042_26646be2_578265.png "屏幕截图.png")
 **用户登录日志** 
![用户日志](https://images.gitee.com/uploads/images/2018/0808/120341_7dee3e03_578265.png "屏幕截图.png")
 **phpMyAdmin** 
![phpMyAdmin](https://images.gitee.com/uploads/images/2018/0730/155553_5cb2fcb7_578265.png "屏幕截图.png")

