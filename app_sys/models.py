# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Server_Install(models.Model):
    depend_name = models.CharField(max_length=64)
    depend_version = models.CharField(max_length=64,null=True)
    install_script = models.TextField(null=True)
    def __unicode__(self):
        return self.service_name
